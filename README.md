# Setup
## Install Go
> https://golang.org/dl/

## Install otpauth
```bash
go get github.com/dim13/otpauth
```

## Prepare your export.txt file
> list your **otpauth-migration://offline?data=X** links (one per line)

## Run!
> ./export.sh > import.txt