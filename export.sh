#!/bin/bash
while IFS= read -r line
do
  OUT=($(otpauth -link "$line"))

  for var in "${OUT[@]}"
  do
    echo "${var}"
  done
done < "export.txt"